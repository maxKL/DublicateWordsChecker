import com.maximov.parsing.Parser;
import com.maximov.storage.UniqueWordsStorage;
import com.maximov.utils.*;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentCaptor;
import org.mockito.MockitoAnnotations;
import org.mockito.*;

import java.io.IOException;
import java.util.List;

import static com.maximov.resources.StringConstants.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ParserTest {
    @Spy
    private UniqueWordsStorage spyUniqueWordsStorage = UniqueWordsStorage.getInstance();
    @Spy
    private Printer spyPrinter = new Printer();

    public ParserTest(){
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown(){
        spyUniqueWordsStorage.reset();
        Parser.resetStopFlag();
    }

    @Test
    public void init_InvalidResourceLocation_ThrowsIOException(){
        assertThrows(IOException.class, () -> new Parser("incorrectResource.txt"));
    }

    @Test
    public void run_TextWithoutDuplicates_AddingWordsToStorage() throws IOException {
        ResourceReader mockReader = mock(ResourceReader.class);
        when(mockReader.readLine())
                .thenReturn("Строка со словами")
                .thenReturn("И ещё!")
                .thenReturn(null);

        Parser parser = new Parser(mockReader, spyUniqueWordsStorage, spyPrinter);
        parser.run();

        verify(spyUniqueWordsStorage).tryPut("Строка");
        verify(spyUniqueWordsStorage).tryPut("со");
        verify(spyUniqueWordsStorage).tryPut("словами");
        verify(spyUniqueWordsStorage).tryPut("И");
        verify(spyUniqueWordsStorage).tryPut("ещё");
    }

    @Test
    public void run_TextWithoutDuplicates_PrintWords() throws IOException {
        ResourceReader mockReader = mock(ResourceReader.class);
        when(mockReader.readLine())
                .thenReturn("Строка со словами")
                .thenReturn("И ещё!")
                .thenReturn(null);

        Parser parser = new Parser(mockReader, spyUniqueWordsStorage, spyPrinter);
        parser.run();

        verify(spyPrinter).print("Строка");
        verify(spyPrinter).print("со");
        verify(spyPrinter).print("словами");
        verify(spyPrinter).print("И");
        verify(spyPrinter).print("ещё");
    }

    @Test
    public void run_TextWithDuplicates_AddingWordsToStorage() throws IOException {
        ResourceReader mockReader = mock(ResourceReader.class);
        when(mockReader.readLine())
                .thenReturn("Строка со словами и ещё словами.")
                .thenReturn("Много короче слов.")
                .thenReturn(null);

        Parser parser = new Parser(mockReader, spyUniqueWordsStorage, spyPrinter);
        parser.run();

        verify(spyUniqueWordsStorage).tryPut("Строка");
        verify(spyUniqueWordsStorage).tryPut("со");
        verify(spyUniqueWordsStorage, times(2)).tryPut("словами");
        verify(spyUniqueWordsStorage).tryPut("и");
        verify(spyUniqueWordsStorage).tryPut("ещё");
    }

    @Test
    public void run_TextWithDuplicates_PrintDuplicateException() throws IOException {
        ResourceReader mockReader = mock(ResourceReader.class);
        when(mockReader.readLine()).thenReturn("Строка со словами и ещё словами. Много короче слов.").thenReturn(null);

        Parser parser = new Parser(mockReader, spyUniqueWordsStorage, spyPrinter);
        parser.run();

        ArgumentCaptor<String> valueCaptor = ArgumentCaptor.forClass(String.class);
        verify(spyPrinter, times(6)).print(valueCaptor.capture());
        List<String> values = valueCaptor.getAllValues();
        assertTrue(values.get(5).contains(DUPLICATE_EXCEPTION));
        assertTrue(values.get(5).contains("словами"));
    }

    @Test
    public void run_TextWithInvalidWord_PrintInvalidWordException() throws IOException {
        ResourceReader mockReader = mock(ResourceReader.class);
        when(mockReader.readLine()).thenReturn("Строка с invalid слово.").thenReturn(null);

        Parser parser = new Parser(mockReader, spyUniqueWordsStorage, spyPrinter);
        parser.run();

        ArgumentCaptor<String> valueCaptor = ArgumentCaptor.forClass(String.class);
        verify(spyPrinter, times(1)).print(valueCaptor.capture());
        List<String> values = valueCaptor.getAllValues();
        assertTrue(values.get(0).contains(INVALID_WORD_EXCEPTION));
        assertTrue(values.get(0).contains("invalid"));
    }

    @Test
    public void run_ReaderThrowsIOException_PrintReadSourceException() throws IOException {
        ResourceReader mockReader = mock(ResourceReader.class);
        doThrow(IOException.class).when(mockReader).readLine();
        String fakeResourceLocation = "fakeResourceLocation";
        when(mockReader.getResourceLocation()).thenReturn(fakeResourceLocation);

        Parser parser = new Parser(mockReader, spyUniqueWordsStorage, spyPrinter);
        parser.run();

        verify(spyPrinter).print(String.format(READ_RESOURCE_EXCEPTION, fakeResourceLocation));
        verify(mockReader).close();
    }

    @Test
    public void run_ReaderThrowsException_PrintReadSourceException() throws IOException {
        ResourceReader mockReader = mock(ResourceReader.class);
        doThrow(Exception.class).when(mockReader).readLine();
        String fakeResourceLocation = "fakeResourceLocation";
        when(mockReader.getResourceLocation()).thenReturn(fakeResourceLocation);

        Parser parser = new Parser(mockReader, spyUniqueWordsStorage, spyPrinter);
        parser.run();

        verify(spyPrinter).print(UNEXPECTED_EXCEPTION);
        verify(mockReader).close();
    }
}