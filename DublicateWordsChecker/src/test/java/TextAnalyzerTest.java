import com.maximov.exceptions.ParseException;
import com.maximov.parsing.TextAnalyzer;
import org.junit.jupiter.api.*;
import java.util.Arrays;

import static com.maximov.resources.StringConstants.INVALID_WORD_EXCEPTION;
import static org.junit.jupiter.api.Assertions.*;

class TextAnalyzerTest {
    private TextAnalyzer textAnalyzer;

    @BeforeEach
    public void init(){
        textAnalyzer = new TextAnalyzer();
    }

    @Test
    public void getWords_TextWithUniqueCyrillicWords_DefaultBehaviour() throws ParseException {
        String text = "Шла \"Саша\" по шоссе  и сосала    какую-то 'сушку'";

        String[] result = textAnalyzer.getWords(text);

        String[] expectedResult = {"Шла", "Саша", "по", "шоссе", "и", "сосала", "какую-то", "сушку"};
        assertTrue(Arrays.deepEquals(expectedResult, result));
    }

    @Test
    public void getWords_TextWithUniqueCyrillicWordsAndPunctuationMarks_IgnorePunctuationMarks() throws ParseException {
        String text = "Шла. Саша, по! шоссе? и : ; сосала — сушку";

        String[] result = textAnalyzer.getWords(text);

        String[] expectedResult = {"Шла", "Саша", "по", "шоссе", "и", "сосала", "сушку"};
        assertTrue(Arrays.deepEquals(expectedResult, result));
    }

    @Test
    public void getWords_TextWithNumbers_DefaultBehaviour() throws ParseException {
        String text = "Шла Саша по автостраде 60 и сосала 2 сушки.";

        String[] result = textAnalyzer.getWords(text);

        String[] expectedResult = {"Шла", "Саша", "по", "автостраде", "60", "и", "сосала", "2", "сушки"};
        assertTrue(Arrays.deepEquals(expectedResult, result));
    }

    @Test
    public void getWords_TextWithNegativeNumbers_DefaultBehaviour() throws ParseException {
        String text = "На улице было -3. Завтра обещали -10.";

        String[] result = textAnalyzer.getWords(text);

        String[] expectedResult = {"На", "улице", "было", "-3", "Завтра", "обещали", "-10"};
        assertTrue(Arrays.deepEquals(expectedResult, result));
    }

    @Test
    public void getWords_TextContainsInvalidLatinWord_ThrowsParseException() throws ParseException {
        getWords_TextContainsInvalidWord("invalidLatinWord");
    }

    @Test
    public void getWords_TextContainsInvalidNumberWord1_ThrowsParseException() throws ParseException {
        getWords_TextContainsInvalidWord("12-");
    }

    @Test
    public void getWords_TextContainsInvalidNumberWord2_ThrowsParseException() throws ParseException {
        getWords_TextContainsInvalidWord("12-42");
    }

    @Test
    public void getWords_TextContainsInvalidWord1_ThrowsParseException() throws ParseException {
        getWords_TextContainsInvalidWord("слово-");
    }

    @Test
    public void getWords_TextContainsInvalidWord2_ThrowsParseException() throws ParseException {
        getWords_TextContainsInvalidWord("-слово");
    }

    private void getWords_TextContainsInvalidWord(String invalidWord) throws ParseException {
        String text = String.format("Текст с неправильным словом \"%1s\"", invalidWord);

        Throwable ex = assertThrows(ParseException.class, () -> textAnalyzer.getWords(text));
        String exMessage = ex.getMessage();
        assertTrue(exMessage.contains(INVALID_WORD_EXCEPTION)
                && exMessage.contains(invalidWord));
    }

    @Test
    public void getWords_NullOrEmptyText_EmptyWordsArray() throws ParseException {
        String[] emptyTexts = {null, "", " "};

        for(String text : emptyTexts){
            String[] result = textAnalyzer.getWords(text);
            assertEquals(0, result.length);
        }
    }
}