import com.maximov.storage.UniqueWordsStorage;
import org.junit.jupiter.api.*;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class UniqueWordsStorageTest {
    private static UniqueWordsStorage wordsStorage;
    private static Set internalStorage;

    @BeforeAll
    public static void setUp(){
        wordsStorage = UniqueWordsStorage.getInstance();
        internalStorage = wordsStorage.getInternalStorage();
    }

    @Test
    public void tryPut_UniqueWords_Success(){
        String[] words = {"слово1", "слово2", "слово3"};

        for(int i = 0; i < words.length; i++) {
            UniqueWordsStorage.AddResult result = wordsStorage.tryPut(words[i]);

            assertEquals(UniqueWordsStorage.AddResult.OK, result);
            assertEquals(i + 1, internalStorage.size());
            assertTrue(internalStorage.contains(words[i]));
        }
    }

    @Test
    public void tryPut_DuplicateWords_DuplicateWordResult(){
        String duplicate = "дубликат";
        internalStorage.add(duplicate);
        int initSize = internalStorage.size();

        UniqueWordsStorage.AddResult result = wordsStorage.tryPut(duplicate);

        assertEquals(UniqueWordsStorage.AddResult.DUPLICATE_WORD, result);
        assertTrue(wordsStorage.isStorageLocked());
        assertEquals(initSize, internalStorage.size());
        assertTrue(internalStorage.contains(duplicate));
    }

    @Test
    public void tryPut_StorageIsLocked_StorageLockedResult(){
        int initSize = internalStorage.size();
        wordsStorage.setStorageLock(true);

        UniqueWordsStorage.AddResult result = wordsStorage.tryPut("слово");

        assertEquals(UniqueWordsStorage.AddResult.STORAGE_LOCKED, result);
        assertEquals(initSize, internalStorage.size());
    }

    @AfterEach
    public void TearDown(){
        wordsStorage.reset();
    }
}