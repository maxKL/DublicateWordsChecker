package com.maximov.exceptions;

public class ParseException extends Exception {
    public ParseException(String message, String causeString) {
        super(message + ": " + causeString);
    }
}
