package com.maximov.parsing;

import com.maximov.exceptions.ParseException;
import com.maximov.storage.UniqueWordsStorage;
import com.maximov.utils.Printer;
import com.maximov.utils.ResourceReader;
import org.apache.log4j.Logger;

import java.io.*;

import static com.maximov.resources.StringConstants.*;

/**
 * Производит парсинг текста из конкретного ресурса.
 * В случае нахождения дублирующегося слова или цифрового сочитания, либо запрещённых символов
 * немедленно завершит работу и выбросит соответствующее исключение.
 * При этом пошлёт сигнал остановки всем остальным потокам.
 */
public class Parser implements Runnable {
    private UniqueWordsStorage wordsStorage;
    private TextAnalyzer textAnalyzer = new TextAnalyzer();
    private Printer printer = new Printer();
    private ResourceReader resourceReader;
    private static boolean stopFlag;
    private static Object lockObject = new Object();
    private static Logger logger = Logger.getLogger(Parser.class);

    private Parser(){
        this.wordsStorage = UniqueWordsStorage.getInstance();
    }

    public Parser(String resourcePath) throws IOException {
        this();
        this.resourceReader = new ResourceReader(resourcePath);
    }

    public Parser(ResourceReader resourceReader, UniqueWordsStorage storage, Printer printer){
        this();
        this.resourceReader = resourceReader;
        this.wordsStorage = storage;
        this.printer = printer;
    }

    @Override
    public void run() {
        try (ResourceReader reader = resourceReader){
            String str;
            while((str=reader.readLine())!=null) {
                String[] words = textAnalyzer.getWords(str);
                if (!tryPutWordsToStorage(words))
                    break;
            }
        } catch (ParseException e) {
            onException(e.getMessage(), e);
        } catch (IOException e) {
            onException(String.format(READ_RESOURCE_EXCEPTION, resourceReader.getResourceLocation()), e);
        } catch (Exception e) {
            onException(UNEXPECTED_EXCEPTION, e);
        }
    }

    private boolean tryPutWordsToStorage(String[] words) throws ParseException {
        for (String word : words) {
            synchronized (lockObject) {
                if(stopFlag)
                    return false;

                UniqueWordsStorage.AddResult result = this.wordsStorage.tryPut(word);
                switch (result) {
                    case OK:
                        this.printer.print(word);
                        logger.trace(String.format("Слово %s добавлено в хранилище", word));
                        break;
                    case DUPLICATE_WORD:
                        throw new ParseException(DUPLICATE_EXCEPTION, word);
                    case STORAGE_LOCKED:
                        return false;
                }
            }
        }
        return true;
    }

    private void onException(String outMessage, Exception e){
        synchronized (lockObject) {
            stopFlag = true;
            printer.print(outMessage);
            logger.error(outMessage, e);
        }
    }

    public static void resetStopFlag(){
        stopFlag = false;
    }
}
