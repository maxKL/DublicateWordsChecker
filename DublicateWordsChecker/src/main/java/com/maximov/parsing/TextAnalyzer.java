package com.maximov.parsing;

import com.maximov.exceptions.*;

import java.util.LinkedList;
import java.util.regex.*;

import static com.maximov.resources.StringConstants.INVALID_WORD_EXCEPTION;

/**
 * Класс для обработки текста
 */
public class TextAnalyzer {

    /**
     * Функция выделяет отдельные слова из строки
     * @param text
     * @return Массив слов
     * @throws ParseException
     */
    public String[] getWords(String text) throws ParseException {
        if(text == null || text.isEmpty())
            return new String[0];

        LinkedList<String> words = new LinkedList<>();
        String[] splittedStrings = text.split("[,.!?:;—\"\']|\\s+");

        for (int i = 0; i < splittedStrings.length; i++){
            if(splittedStrings[i].equals("")) continue;

            if(!checkWord(splittedStrings[i]))
                throw new ParseException(INVALID_WORD_EXCEPTION, splittedStrings[i]);

            words.add(splittedStrings[i]);
        }

        String[] result = new String[words.size()];
        return words.toArray(result);
    }

    private boolean checkWord(String word) {
        Matcher m = Pattern.compile("^[а-яёА-ЯЁ]+$|^[а-яёА-ЯЁ]+-?[а-яёА-ЯЁ]+$|^-?\\d+$").matcher(word);
        return m.matches();
    }
}
