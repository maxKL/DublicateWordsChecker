package com.maximov;

import com.maximov.parsing.Parser;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.IOException;

public class Main {

    static {
        DOMConfigurator.configure("src/main/resources/log4j.xml");
    }

    public static void main(String args []) {
        Thread[] threads = new Thread[args.length];

        for(int i = 0; i < args.length;i++){
            try {
                threads[i] = new Thread(new Parser(args[i]));
            } catch (IOException e) {
                System.out.println("Ресурс по пути \"" + args[i] + "\" не найден или удалён. Проверьте корректность пути.");
                return;
            }
        }

        for(Thread thread : threads){
            thread.start();
        }
    }
}
