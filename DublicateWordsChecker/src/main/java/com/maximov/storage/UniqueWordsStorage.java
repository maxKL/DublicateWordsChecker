package com.maximov.storage;

import java.util.*;

/**
 * Хранилище для хранения уникальных слов. Реализовано с использование паттерна Singleton.
 */
public class UniqueWordsStorage {
    private static UniqueWordsStorage instance = new UniqueWordsStorage();
    private Set<String> internalStorage = new HashSet<>(100);
    private boolean storageLock;

    private UniqueWordsStorage(){

    }

    public Set<String> getInternalStorage(){
        return this.internalStorage;
    }

    public boolean isStorageLocked(){
        return storageLock;
    }

    public void setStorageLock(boolean value){
        this.storageLock = value;
    }

    public void reset(){
        this.internalStorage.clear();
        this.setStorageLock(false);
    }

    public static UniqueWordsStorage getInstance(){
        return instance;
    }

    /**
     * Добавление слова в хранилище
     * @param word
     * @return Результат добавления. Может быть успешным (OK),
     * информирующим о том, что у слова есть дубликат (DUPLICATE_WORD),
     * информирующим о том, что хранилище закрыто для добавления (STORAGE_LOCKED)
     */
    public AddResult tryPut(String word){
        if(storageLock)
            return AddResult.STORAGE_LOCKED;

        boolean result = internalStorage.add(word.toLowerCase());
        if(!result) {
            storageLock = true;
            return AddResult.DUPLICATE_WORD;
        }

        return AddResult.OK;
    }

    public enum AddResult{
        OK, DUPLICATE_WORD, STORAGE_LOCKED
    }
}
