package com.maximov.utils;

import java.io.*;
import java.net.*;

public class ResourceReader implements Closeable {
    private String resourceLocation;
    private InputStream stream;
    private InputStreamReader inputStreamReader;
    private BufferedReader bufferedReader;

    public ResourceReader(String resourceLocation) throws IOException {
        try {
            this.resourceLocation = resourceLocation;
            this.stream = GetRecourseStream(resourceLocation);
            this.inputStreamReader = new InputStreamReader(stream, "utf-8");
            this.bufferedReader = new BufferedReader(inputStreamReader);
        }finally {
            close();
        }
    }

    public String getResourceLocation(){
        return this.resourceLocation;
    }

    public String readLine() throws IOException {
        return this.bufferedReader.readLine();
    }

    private InputStream GetRecourseStream(String recourseLocation) throws IOException {
        URL url = getURL(recourseLocation);
        return (url != null ? url.openStream() : new FileInputStream(recourseLocation));
    }

    private URL getURL(String url){
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    @Override
    public void close() throws IOException {
        if(this.bufferedReader != null) this.bufferedReader.close();
        if(this.inputStreamReader != null) this.inputStreamReader.close();
        if(this.stream != null) this.stream.close();
    }
}
