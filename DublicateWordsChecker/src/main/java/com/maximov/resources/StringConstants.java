package com.maximov.resources;

public class StringConstants {
    public final static String DUPLICATE_EXCEPTION = "Был найден дубликат";
    public final static String UNEXPECTED_EXCEPTION = "Возникла непредвиденная ошибка в приложении.";
    public final static String READ_RESOURCE_EXCEPTION = "Не удалось прочитать данные из ресурса %s.";
    public final static String INVALID_WORD_EXCEPTION = "Недопустимое слово или числовое значение";

    private StringConstants(){}
}
